# checkDelete

Creates a file in a specific directory and then checks a set interval later if it is still in the folder.
This is meant to be used with another file deletion software.