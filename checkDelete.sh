#!/bin/bash
	
while :
do
	#Create a test file that should soon be deleted.
	timeStamp=`date +%Y-%m-%d_%H:%M:%S`
	echo "This file is meant to be deleted. Time Created:" $timeStamp >> /home/picarro/SI2000/Log/RDF/deleteTestFile.txt
	echo $timeStamp: "Test file created" | tee -a checkDeleteOutput.log
	
	#Sleep the program for 10 hours + 5 minutes
	sleep 36300s
	
#Check if file is deleted
	if [ -f /home/picarro/SI2000/Log/RDF/deleteTestFile.txt ]
    then
        echo $timeStamp: " FILE STILL EXISTS" | tee -a checkDeleteOutput.log
    else
        echo $timeStamp: " File has been deleted" | tee -a checkDeleteOutput.log
    fi
    
	sleep 30s
	
done
